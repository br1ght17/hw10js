let a = document.createElement("a")
a.innerHTML = "Learn More"
a.href = "#"
const footer = document.querySelector("footer")
footer.append(a)

let select = document.createElement('select')
select.id = "rating"
const features = document.querySelector(".features")
features.before(select)

for(let i = 4; i >= 1; i--){
    let option = document.createElement('option')
    option.value = i
    option.innerHTML = i + " stars"
    select.appendChild(option)
}